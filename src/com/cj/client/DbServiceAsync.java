package com.cj.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DbServiceAsync {
	public void startMonitor(String email, String url, AsyncCallback<Integer> callback);
	public void stopMonitor(String email, String url, AsyncCallback<Integer> callback);
}
