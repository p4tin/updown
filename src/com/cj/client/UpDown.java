package com.cj.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class UpDown implements EntryPoint {
	public static final String REGEX_EMAIL = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
	public static final String REGEX_URL = "@(https?://([-\\w\\.]+)+(:\\d+)?(/([\\w/_\\.]*(\\?\\S+)?)?)?)@";
	
	Button monitorButton;
	static Label lblFeedback = null;
	static Label lblErr = null;
	
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();
		
		VerticalPanel vp1 = new VerticalPanel();
		vp1.setSize("100%", "100%");
		vp1.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		rootPanel.add(vp1);
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setHeight("");
		vp1.add(horizontalPanel);
		
		HTML html = new HTML("<img src=\"double_arrow.png\" height=\"40px\" width=\"50px\" />", true);
		horizontalPanel.add(html);
		
		Label lblTitle = new Label("UP or DOWN");
		horizontalPanel.add(lblTitle);
		horizontalPanel.setCellVerticalAlignment(lblTitle, HasVerticalAlignment.ALIGN_MIDDLE);
		lblTitle.setStyleName("gwt-Title");
		lblTitle.setSize("274px", "50px");
		
		
		VerticalPanel vp = new VerticalPanel();
		vp.setSize("100%", "100%");
		vp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		vp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		vp1.add(vp);
		
		HTML adFooter = new HTML("<iframe id=\"mainIframe\" src=\"adsense_2.html\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height = \"110px\" width = \"738px\">Iframes not supported.</iframe>", true);
		vp1.add(adFooter);
		adFooter.setSize("738px", "110px");
		
		HTML lblFooter = new HTML("&copy <a href='http://codingjunky.com'>CodingJunky.com</a> + <a href='http://sysgeeks.com'>SysGeeks.com</a>");
		vp1.add(lblFooter);
		lblFooter.setSize("274px", "15px");
		
		
		AbsolutePanel absolutePanel = new AbsolutePanel();
		absolutePanel.setSize("657px", "300px");
		vp.add(absolutePanel);
		
		
		Label lblEmailAddress = new Label("Email Address");
		absolutePanel.add(lblEmailAddress, 24, 83);
		lblEmailAddress.setSize("120px", "15px");
		
		final TextBox emailBox = new TextBox();
		absolutePanel.add(emailBox, 150, 83);
		emailBox.setSize("195px", "20px");
		
		final TextBox urlBox = new TextBox();
		absolutePanel.add(urlBox, 150, 141);
		urlBox.setSize("195px", "20px");
		
		Label lblUrl = new Label("URL");
		absolutePanel.add(lblUrl, 15, 141);
		lblUrl.setSize("75px", "15px");

		monitorButton = new Button();
		absolutePanel.add(monitorButton, 65, 211);
		monitorButton.setText("Start Monitoring");
		
		Button stopButton = new Button();
		stopButton.setText("Stop Monitoring");
		absolutePanel.add(stopButton, 211, 211);
		
		HTML htmliframesNotSupported = new HTML("<iframe id=\"mainIframe\" src=\"adsense_1.html\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height = \"260px\" width = \"260px\">Iframes not supported.</iframe>", true);
		absolutePanel.add(htmliframesNotSupported, 387, 18);
		htmliframesNotSupported.setSize("260px", "260px");
		
		lblErr = new Label("");
		lblErr.setStyleName("gwt-Label-err");
		lblErr.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		absolutePanel.add(lblErr, 20, 28);
		lblErr.setSize("361px", "18px");
		
		lblFeedback = new Label("");
		lblFeedback.setStyleName("gwt-Label-feed");
		lblFeedback.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		absolutePanel.add(lblFeedback, 20, 28);
		lblFeedback.setSize("361px", "18px");
								
		monitorButton.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				//Validate URL
				if(!isValidUrl(urlBox.getText()))
					displayError("Please enter a valid URL");
				else if(!emailBox.getText().matches(REGEX_EMAIL))
					displayError("Please enter a valid Email Address");
				else
					startMonitor(emailBox.getText(), urlBox.getText());
			}
		});
		
		stopButton.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				if(!isValidUrl(urlBox.getText()))
					displayError("Please enter a valid URL");
				else if(!emailBox.getText().matches(REGEX_EMAIL))
					displayError("Please enter a valid Email Address");
				else
					stopMonitor(emailBox.getText(), urlBox.getText());
				//Validate URL
			}
		});
	}
	
	private void startMonitor(String email, String url) {
		final String furl = url;
        DbService.Util.getInstance().startMonitor(email, url, new AsyncCallback<Integer>() {
            public void onSuccess(Integer result) {
            	if(result == 1)
            		displayFeedback("Started monitoring: " + furl + ".");
            	else if(result == 2)
            		displayFeedback("You are already monitoring: " + furl + ".");
            	else
            		displayError("Error 98- Could not complete request");
            }
            public void onFailure(Throwable caught) {
            	displayError("Error 99 - Could not complete request");
            }
        });
    }
	
	private void stopMonitor(String email, String url) {
		final String furl = url;
        DbService.Util.getInstance().stopMonitor(email, url, new AsyncCallback<Integer>() {
            public void onSuccess(Integer result) {
            	if(result == 1)
            		displayFeedback("Stopped monitoring: " + furl + ".");
            	else if(result == 2)
            		displayError("You are not monitoring " + furl + ".");
            	else if(result == 3)
            		displayError("Site not found in database.");
            	else
            		displayError("Error 98b- Could not complete request");
            }
            public void onFailure(Throwable caught) {
            	displayError("Server Error - Could not complete request");
            }
        });
    }
	
	private void displayFeedback(String msg) {
    	lblFeedback.setText(msg);
    	Timer t = new Timer() {
    	    public void run() {
    	    	lblFeedback.setText("");
    	    }
    	};
    	t.schedule(5000);
	}
	
	private void displayError(String msg) {
    	lblErr.setText(msg);
    	Timer t = new Timer() {
    	    public void run() {
    	    	lblErr.setText("");
    	    }
    	};
    	t.schedule(5000);
	}

	
	// Java method
	public native boolean isValidUrl(String url) /*-{
	    var pattern = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	    return pattern.test(url);
	}-*/; 
}
