package com.cj.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("DbService")
public interface DbService extends RemoteService {
	/**
	 * Utility class for simplifying access to the instance of async service.
	 */
	public static class Util {
		private static DbServiceAsync instance;
		public static DbServiceAsync getInstance(){
			if (instance == null) {
				instance = GWT.create(DbService.class);
			}
			return instance;
		}
	}
	
	public int startMonitor(String email, String url);
	public int stopMonitor(String email, String url);
}
