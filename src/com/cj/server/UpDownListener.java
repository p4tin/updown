package com.cj.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.*;
import com.db4o.ObjectSet;


public class UpDownListener implements ServletContextListener {

	private ServletContext context = null;
	Monitoring mon = null;

	/*
	 * This method is invoked when the Web Application has been removed and is
	 * no longer able to accept requests
	 */

	public void contextDestroyed(ServletContextEvent event) {
		// Output a simple message to the server's console
		mon.stopIt();
		DbManager dbm = (DbManager)event.getServletContext().getAttribute("dbm");
		dbm.close();

		this.context = null;
		System.out.println("The Simple Web App. Has Been Removed");
	}

	// This method is invoked when the Web Application
	// is ready to service requests

	public void contextInitialized(ServletContextEvent event) {
		// Read properties file.
		Properties props = new Properties();
		try {
		    props.load(new FileInputStream("/var/UpDown/UpDown.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		int interval = new Integer(props.getProperty("Monitor.Interval"));
		
		System.out.println("Interval = " + interval);
		
		this.context = event.getServletContext();
		this.context.setAttribute("sites", null);
		this.context.setAttribute("running", "0");
		DbManager dbm = new DbManager();
		this.context.setAttribute("dbm", dbm);
		
		mon = new Monitoring(this.context, interval);
        mon.start();

        // Output a simple message to the server's console
		System.out.println("The Simple Web App. Is Ready");

	}
}

class Monitoring {
    private final Timer timer = new Timer();
    private final int seconds;
    private static ServletContext con;
    
    public Monitoring(ServletContext c, int seconds) {
    	con = c;
        this.seconds = seconds;
    }

    public void start() {
        timer.schedule(new TimerTask() {
            public void run() {
            	if("1".equals(con.getAttribute("running"))) {
            		System.out.println("The Monitoring task is already running...  Exiting now.");
            		return;
            	}
            	con.setAttribute("running", "1");
                monitor();
                //timer.cancel();
                con.setAttribute("running", "0");
            }
            
            private void monitor() {
                System.out.println(new Date() + " - Monitoring ");
                DbManager dbm = (DbManager)con.getAttribute("dbm");
        		ObjectSet<Site> sites = dbm.db.queryByExample(new Site(null, null));
        		while (sites.hasNext()) {
        			Site site = (Site) sites.next();
					System.out.print("\t" + site.getUrl() + ": ");

					WebFile wf = new WebFile(site.getUrl());
					if(wf.getResponseCode() != 200) {
						System.out.println("Offline");
						dbm.setSiteStatus(site.getUrl(), 0);
					}
					else {
						System.out.println("OK!");
						dbm.setSiteStatus(site.getUrl(), 1);
					}
				}
    		}
        }, 0, seconds * 1000);
    }

    public void stopIt() {
    	System.out.println("Cancelling Task!");
    	timer.cancel();
    	System.out.println("Done..");
    }

}