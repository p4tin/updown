package com.cj.server;

import com.db4o.*;
import com.db4o.config.EmbeddedConfiguration;

public class DbManager {
	ObjectContainer db = null;

	public DbManager() {
		try {
			EmbeddedConfiguration config =
				Db4oEmbedded.newConfiguration(); 
			config.common().objectClass("com.cj.server.Site").cascadeOnUpdate(true);
			config.common().objectClass("com.cj.server.Site").cascadeOnDelete(true);
			config.common().objectClass("com.cj.server.Site").updateDepth(10);

			db = Db4oEmbedded.openFile(config, "/var/UpDown/db4o_sites.db");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void listResult(ObjectSet<Site> result) { 
		System.out.println(result.size()); 
		while(result.hasNext()) {
			System.out.println(result.next());
		}
	}
	
	public int addEmailToSite(String url, String email) {
		// Add an Email / Site
		ObjectSet<Site> set = null;
		if ((set = db.queryByExample(new Site(null, url))).hasNext()) {
			// Yes update it
			Site s = (Site) set.next();
			if (s.getEmails().contains(email) == false) {
				s.addEmail(email);
				db.store(s);
				db.commit();
				return 1;
			} else {
				System.out.println("Asked to add " + email + " from " + url
						+ " but It was already there.");
				return 2;
			}

		} else {
			// Nope, go ahead and add it
			db.store(new Site(email, url));
			db.commit();
			return 1;
		}
	}

	public int removeEmailFromSite(String url, String email) {
		// Remove an Email / Site
		ObjectSet<Site> set = null;
		if ((set = db.queryByExample(new Site(null, url))).hasNext()) {
			// Yes update it
			Site s = (Site) set.next();
			if (s.getEmails().contains(email) == true) {
				s.removeEmail(email);
				if (s.getEmails().size() == 0)
					db.delete(s);
				else
					db.store(s);
				db.commit();
				return 1;
			} else {
				System.out.println("Asked to remove " + email + " from " + url
						+ " but It was not there.");
				return 2;
			}
		} else {
			System.out.println("Asked to removed " + email + " from " + url
					+ " but I did not find the site in my database.");
			return 3;
		}
	}

	public void setSiteStatus(String url, int status) {
		ObjectSet<Site> set = null;
		if ((set = db.queryByExample(new Site(null, url))).hasNext()) {
			// Yes update it
			Site s = (Site) set.next();
			s.setStatus(status);
			db.store(s);
			db.commit();
		}
	}

	public void close() {
		if (db != null)
			db.close();
	}

	public static void main(String[] args) {
		//DbManager dbm = new DbManager();
	}
}