package com.cj.server;

import javax.mail.MessagingException;

import com.cj.client.DbService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class DbServiceImpl extends RemoteServiceServlet implements DbService {
	private static final long serialVersionUID = 1L;
	
	@Override
	public int startMonitor(String email, String url) {
		System.out.println("Saving: " + email + " / " + url);
		DbManager dbm = (DbManager)this.getServletContext().getAttribute("dbm");
		int ret = dbm.addEmailToSite(url, email);
		if(ret == 1) {
		String[] sendTo = { email };
			try {
				new SendMail().send(
					sendTo, 
					"Start UpDown - Confirmation Email", "Thanks you for choosing us, we will now start monitoring your site: " + url, 
					"updown@sysgeeks.com"
				);
			} catch (MessagingException e) {
				e.printStackTrace();
				return 99;
			}
		}
		return ret;
	}

	@Override
	public int stopMonitor(String email, String url) {
		System.out.println("Saving: " + email + " / " + url);
		DbManager dbm = (DbManager)this.getServletContext().getAttribute("dbm");
		int ret = dbm.removeEmailFromSite(url, email);
		if(ret == 1) {
			String[] sendTo = { email };
			try {
				new SendMail().send(
					sendTo, 
					"Stop UpDown - Confirmation Email", "Sorry to see you go, we will now stop monitoring your site: " + url, 
					"updown@sysgeeks.com"
				);
			
			} catch (MessagingException e) {
				e.printStackTrace();
				return 99;
			}
		}
		return ret;
	}
}
