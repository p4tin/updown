package com.cj.server;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;

public class DbDumper {
	static ObjectContainer db = null;

	public static void listResult(ObjectSet<Site> result) { 
		System.out.println(result.size()); 
		while(result.hasNext()) {
			System.out.println(result.next());
		}
	}

	public static void main(String[] args) {
		try {
			EmbeddedConfiguration config =
				Db4oEmbedded.newConfiguration(); 
			config.common().objectClass("com.cj.server.Site").cascadeOnUpdate(true);
			config.common().objectClass("com.cj.server.Site").cascadeOnDelete(true);
			config.common().objectClass("com.cj.server.Site").updateDepth(10);

			db = Db4oEmbedded.openFile(config, "/var/UpDown/db4o_sites.db");

			ObjectSet<Site> set = db.queryByExample(new Site(null,null));
			listResult(set);
		
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
