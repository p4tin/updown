package com.cj.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

	private static int SMTP_SSL = 1;
	private static String SMTP_HOST_NAME = "smtp.gmail.com";
	private static String SMTP_PORT = "465";
	private static String SMTP_USER = "updown@sysgeeks.com";
	private static String SMTP_PASSWORD = "iv17wt0c";
	private static final String emailMsgTxt = "Test Message Contents";
	private static final String emailSubjectTxt = "A test from gmail";
	private static String emailFromAddress = "UpDown@sysgeeks.com";
	private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	private static final String[] sendTo = { "paul@codingjunky.com" };

	public static void main(String args[]) throws Exception {
		new SendMail().send(sendTo, emailSubjectTxt, emailMsgTxt,
				emailFromAddress);
		System.out.println("Sucessfully Sent mail to All Users");
	}
	
	public SendMail() {
		//Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		Properties props = new Properties();
		try {
		    props.load(new FileInputStream("/var/UpDown/UpDown.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		emailFromAddress = props.getProperty("FROM");
		SMTP_SSL = new Integer(props.getProperty("SMTP_SSL"));
		SMTP_HOST_NAME = props.getProperty("SMTP_SERVER");
		SMTP_PORT = props.getProperty("SMTP_PORT");
		SMTP_USER = props.getProperty("SMTP_USER");
		SMTP_PASSWORD = props.getProperty("SMTP_PASSWORD");
	}

	public void send(String recipients[], String subject,
			String message, String from) throws MessagingException {
		if(SMTP_SSL == 1)
			sendSSLMessage(recipients, subject, message, emailFromAddress);
		else
			sendMessage(recipients, subject, message, emailFromAddress);
	}
	
	
	private void sendSSLMessage(String recipients[], String subject,
			String message, String from) throws MessagingException {

		Properties props = new Properties();
		props.put("mail.smtp.host", SMTP_HOST_NAME);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", SMTP_PORT);
		props.put("mail.smtp.socketFactory.port", SMTP_PORT);
		props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.put("mail.smtp.socketFactory.fallback", "false");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(SMTP_USER, SMTP_PASSWORD);
			}
		});

		session.setDebug(true);

		Message msg = new MimeMessage(session);
		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);

		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/plain");
		Transport.send(msg);
	}
	
	
	private void sendMessage(String recipients[], String subject,
			String message, String from) throws MessagingException {
		
		boolean debug = false;

	     //Set the host smtp address
	     Properties props = new Properties();
	     props.put("mail.smtp.host", SMTP_HOST_NAME);

	    // create some properties and get the default Session
	    Session session = Session.getDefaultInstance(props, null);
	    session.setDebug(debug);

	    // create a message
	    Message msg = new MimeMessage(session);

	    // set the from and to address
	    InternetAddress addressFrom = new InternetAddress(emailFromAddress);
	    msg.setFrom(addressFrom);

	    InternetAddress[] addressTo = new InternetAddress[recipients.length]; 
	    for (int i = 0; i < recipients.length; i++)
	    {
	        addressTo[i] = new InternetAddress(recipients[i]);
	    }
	    msg.setRecipients(Message.RecipientType.TO, addressTo);
	   

	    // Optional : You can also set your custom headers in the Email if you Want
	    //msg.addHeader("MyHeaderName", "myHeaderValue");

	    // Setting the Subject and Content Type
	    msg.setSubject(subject);
	    msg.setContent(message, "text/plain");
	    Transport.send(msg);
	}

}