package com.cj.server;

import java.util.Iterator;
import java.util.Vector;

import javax.mail.MessagingException;

public class Site {
	private int status;
	private Vector<String> emails;
	private String url;
	
	public Site() {}
	
	public Site(String email, String url) {
		emails = new Vector<String>();
		this.emails.add(email);
		this.url = url;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int s) {
		if(this.status != s) {
			this.status = s;
			sendNotification();
		}
	}
	
	public void addEmail(String email) {
		emails.add(email);
	}
	public void removeEmail(String email) {
		emails.remove(email);
	}
	
	public Vector<String> getEmails() {
		return emails;
	}
	public void setEmails(Vector<String> emails) {
		this.emails = emails;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	private void sendNotification() {
		String[] sendTo = new String[emails.size()];
		int i = 0;
		for (Iterator<String> iterator = emails.iterator(); iterator.hasNext();) {
			String em = iterator.next();
			sendTo[i++] = em;
		}
		String statusMsg = "Good news, I was able to connect to your site (" + url + ")";
		String titleMsg = "UpDown - " + url + " is Up";
		if(status == 0) {
			statusMsg = "You should check your site (" + url + "), I was not able to connect to it.";
			titleMsg = "UpDown - " + url + " is Down";
		}
		try {
			new SendMail().send(
					sendTo, titleMsg, statusMsg, "updown@sysgeeks.com"
				);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		String ret = url + "(" + status + ")";
		for (Iterator<String> iterator = emails.iterator(); iterator.hasNext();) {
			String em = iterator.next();
			ret = ret + "\n\t" + em;
		}
		return ret;
	}
	
    public boolean equals(Object rhs)
    {
        if (rhs == this)
            return true;
        
        if (!(rhs instanceof Site))
            return false;
        
        Site other = (Site)rhs;
        return (this.url.equals(other.url));
    }
}
