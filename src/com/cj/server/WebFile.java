package com.cj.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public final class WebFile {
    // Saved response.
    private java.util.Map<String,java.util.List<String>> responseHeader = null;
    private java.net.URL responseURL = null;
    private int responseCode = -1;
    private String MIMEtype  = null;
    private Object content   = null;
 
    /** Open a web file. */
    public WebFile( String urlString ) {
        // Open a URL connection.
        java.net.URL url = null;
		try {
			url = new java.net.URL( urlString );
		} catch (MalformedURLException e1) {
			responseCode = 0;
		}
        java.net.URLConnection uconn = null;
		try {
			uconn = url.openConnection( );
		} catch (IOException e1) {
			responseCode = 0;
		}
        if ( !(uconn instanceof java.net.HttpURLConnection) )
            throw new java.lang.IllegalArgumentException(
                "URL protocol must be HTTP." );
        final java.net.HttpURLConnection conn =
            (java.net.HttpURLConnection)uconn;
 
        try {
			conn.connect( );
		} catch (IOException e) {
			responseCode = 0;
		}
 
        try {
			responseCode      = conn.getResponseCode( );
			content = conn.getContent();
		} catch (IOException e) {
			e.printStackTrace();
			responseCode = 0;
		}
        conn.disconnect( );
    }
 
    /** Get the content. */
    public Object getContent( ) {
        return content;
    }
 
    /** Get the response code. */
    public int getResponseCode( ) {
        return responseCode;
    }
 
    /** Get the response header. */
    public java.util.Map<String,java.util.List<String>> getHeaderFields( ) {
        return responseHeader;
    }
 
    /** Get the URL of the received page. */
    public java.net.URL getURL( ) {
        return responseURL;
    }
 
    /** Get the MIME type. */
    public String getMIMEType( ) {
        return MIMEtype;
    }
    
    public static void main(String[] args)
	{
		WebFile wf = new WebFile("http://nationnews.com");
		System.out.println(wf.getResponseCode());
		
		String needle = "Nation Publishing Co";
		Object haystack = wf.getContent();
		System.out.println(haystack.toString());
//		if(haystack.contains(needle)) {
//			System.out.println("Found it");
//		} else {
//			System.out.println("Not here");
//		}		
	}
}